# README #

This README is of School Project in Web Services from Reykjavik University.

### Contributors ###

Gunnar Marteinsson
Sigurður Már Atlason

### Assignment 5 ###

In this assignment, you should use the code from last week, with the following modifications to the /api/courses?semester={semester} GET method:

### (50%) Add language support ###

* A course template should store their names both in English and Icelandic
* When requesting a list of courses, the application should inspect the "accept-language" header, and return the appropriate name of the course, depending on which   language is being requested (Icelandic/English).

### (50%) Add paging support ###

When requesting a list of courses, the application should return max 10 courses. The list should be returned in an envelope, i.e. the return value should be an object with two properties:

1.  Items -> the list of courses
2. Paging -> an object with the following properties:

* PageCount ->should store the number of pages
* PageSize -> the number of items in each page (10 in our case)
* PageNumber -> a 1-based index of the current page being returned
* TotalNumberOfItems -> the total number of items in the collection