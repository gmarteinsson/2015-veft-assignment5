﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoursesAPI.Models;

namespace CoursesAPI.Services.Services
{
    /// <summary>
    /// class envelope which is an object including a list and pageInfo
    /// </summary>
    public class Envelope
    {
        public List<CourseInstanceDTO> Items { get; set; }

        public PageInfo Paging { get; set; }
        
         
    }
}
