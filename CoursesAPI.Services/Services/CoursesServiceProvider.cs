﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoursesAPI.Models;
using CoursesAPI.Services.DataAccess;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Models.Entities;

namespace CoursesAPI.Services.Services
{
    public class CoursesServiceProvider
	{
		private readonly IUnitOfWork _uow;

		private readonly IRepository<CourseInstance> _courseInstances;
		private readonly IRepository<TeacherRegistration> _teacherRegistrations;
		private readonly IRepository<CourseTemplate> _courseTemplates; 
		private readonly IRepository<Person> _persons;

        private const string LangUS = "en-US";

        public CoursesServiceProvider(IUnitOfWork uow)
		{
			_uow = uow;

			_courseInstances      = _uow.GetRepository<CourseInstance>();
			_courseTemplates      = _uow.GetRepository<CourseTemplate>();
			_teacherRegistrations = _uow.GetRepository<TeacherRegistration>();
			_persons              = _uow.GetRepository<Person>();
		}


        /// <summary>
        /// You should implement this function, such that all tests will pass.
        /// </summary>
        /// <param name="courseInstanceID">The ID of the course instance which the teacher will be registered to.</param>
        /// <param name="model">The data which indicates which person should be added as a teacher, and in what role.</param>
        /// <returns>Should return basic information about the person.</returns>
        public PersonDTO AddTeacherToCourse(int courseInstanceID, AddTeacherViewModel model)
		{
			var course = _courseInstances.All().SingleOrDefault(x => x.ID == courseInstanceID);
			if (course == null)
			{
				throw new AppObjectNotFoundException(ErrorCodes.INVALID_COURSEINSTANCEID);
			}

			// TODO: implement this logic!
			return null;
		}

		/// <summary>
		/// You should write tests for this function. You will also need to
		/// modify it, such that it will correctly return the name of the main
		/// teacher of each course.
		/// </summary>
		/// <param name="semester"></param>
		/// <param name="page">1-based index of the requested page.</param>
		/// <returns></returns>
		public Envelope GetCourseInstancesBySemester(string language, string semester = null, int page = 1)
		{
		    const int PAGESIZE = 10;
            
			if (string.IsNullOrEmpty(semester))
			{
				semester = "20153";
			}

		    var courses = (from c in _courseInstances.All()
		        join ct in _courseTemplates.All() on c.CourseID equals ct.CourseID
		        where c.SemesterID == semester
		        select new CourseInstanceDTO
		        {
                    // Name returns Icelandic name by default but English if it is set to English
		            Name = language == LangUS ? ct.NameEN : ct.Name,
		            TemplateID = ct.CourseID,
		            CourseInstanceID = c.ID,
		            MainTeacher = "" // Hint: it should not always return an empty string!
		        }).ToList();
            
            var envelope = new Envelope
            {
                // returns PAGESIZE many items on each page and can skip over pages
                Items = courses.Skip(page * PAGESIZE).Take(PAGESIZE).ToList(),
                Paging = new PageInfo()
                {
                    // take the ceiling of number of courses devided by page size
                    PageCount = (int)Math.Ceiling((decimal)courses.Count() / PAGESIZE),

                    // page size
                    PageSize = PAGESIZE,

                    // page number is the number on the page param
                    PageNumber = page,

                    // number of items is the number of courses in the list
                    TotalNumberOfItems = courses.Count()
                }
            };
            return envelope;
		}
	}
}
