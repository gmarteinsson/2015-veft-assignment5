﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoursesAPI.Models;

namespace CoursesAPI.Services.Services
{
    public class Envelope<T>
    {
        public T Items { get; set; }

        class PageInfo
        {
            public int PageCount          { get; set; }
            public int PageSize           { get; set; }
            public int PageNumber         { get; set; }
            public int TotalNumberOfItems { get; set; }
        }
        
        //public PagingInfo Paging { get; set; }
        
         
    }
}
